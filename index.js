const express = require('express');
const cors = require('cors');
const app = express();
const port = 4043;

app.use(cors({
    origin: '*'   
}));


app.get("/",(req,res)=> {
    res.send({
        "respuesta": "Funciona"
    })
} );

app.get("/cars",(req,res)=> {
    res.send([{
        "codigo" : "001",
         "modelo": "YARIS CROSS",
         "marca" :"TOYOTA",
         "año"   : "2023",
         "precio": "$ 29.23",
         "concesionario"  : "KOBE MOTORS, C.A",
         "cant_disponible":"5",
         "ventas" :"1"
    },
    {
        "codigo" : "002",
         "modelo": "YARIS HB",
         "marca" :"TOYOTA",
         "año"   : "2024",
         "precio": "$ 25.186",
         "concesionario"  : "KANSEI MOTORS, C.A",
         "cant_disponible":"6",
         "ventas" : "2"
    },
    {
        "codigo" : "003",
         "modelo": "YARIS SD",
         "marca" :"TOYOTA",
         "año"   : "2023",
         "precio": "$ 26.216",
         "concesionario"  : "DISTRIBUIDORA LUMO C.A",
         "cant_disponible":"",
         "ventas" : ""
    },
    {
        "codigo" : "004",
         "modelo": "BOLT EUV ",
         "marca" :"CHEVROLET",
         "año"   : "2023",
         "precio": "$26.600",
         "concesionario"  : "KOBE MOTORS, C.A",
         "cant_disponible":"8",
         "ventas" : "3"
    },
    {
        "codigo" : "005",
         "modelo": "BLAZER",
         "marca" :"CHEVROLET",
         "año"   : "2024",
         "precio": "$44,945",
         "concesionario"  : "KANSEI MOTORS, C.A",
         "cant_disponible":"5",
         "ventas" : "2"
    },
    {
        "codigo" : "006",
         "modelo": "TRAVERSE",
         "marca" :"CHEVROLET",
         "año"   : "2024",
         "precio": "$50,495",
         "concesionario"  : "DISTRIBUIDORA LUMO C.A",
         "cant_disponible":"3",
         "ventas" : "1"
    },
    {
        "codigo" : "007",
         "modelo": "cx-30",
         "marca" :"MAZDA",
         "año"   : "2023",
         "precio": "$24.996",
         "concesionario"  : "KOBE MOTORS, C.A",
         "cant_disponible":"6",
         "ventas" : "3"
    },
    {
        "codigo" : "008",
         "modelo": "cx-5 ",
         "marca" :"MAZDA",
         "año"   : "2024",
         "precio": "$  27.500",
         "concesionario"  : "KANSEI MOTORS, C.A",
         "cant_disponible":"2",
         "ventas" : "1"
    },
    {
        "codigo" : "009",
         "modelo": "cx-50 ",
         "marca" :"MAZDA",
         "año"   : "2024",
         "precio": "$30.000",
         "concesionario"  : "DISTRIBUIDORA LUMO C.A",
         "cant_disponible":"4",
         "ventas" : "2"
    },
    {
        "codigo" : "010",
         "modelo": "COROLLA SEG ",
         "marca" :"TOYOTA",
         "año"   : "2023",
         "precio": "33.015",
         "concesionario"  : "KOBE MOTORS, C.A",
         "cant_disponible":"5",
         "ventas" : "3"
    },
    {
        "codigo" : "011",
         "modelo": "COROLLA CROSS",
         "marca" :"TOYOTA",
         "año"   : "",
         "precio": "38.516",
         "concesionario"  : "KANSEI MOTORS, C.A",
         "cant_disponible":"6",
         "ventas" : "2"
    },
    {
        "codigo" : "012",
         "modelo": "HILUX DIÉSEL",
         "marca" :"TOYOTA",
         "año"   : "2024",
         "precio": " $ 42.962",
         "concesionario"  : "DISTRIBUIDORA LUMO C.A",
         "cant_disponible":"4",
         "ventas" : "1"
    },
    {
        "codigo" : "013",
         "modelo": "BOLT EV",
         "marca" :"CHEVROLET",
         "año"   : "2024",
         "precio": "$26,500",
         "concesionario"  : "KOBE MOTORS, C.A",
         "cant_disponible":"3",
         "ventas" : "2"
    },
    {
        "codigo" : "014",
         "modelo": "BOLT EUV ",
         "marca" :"CHEVROLET",
         "año"   : "2023",
         "precio": "$27,800",
         "concesionario"  : "KANSEI MOTORS, C.A",
         "cant_disponible":"6",
         "ventas" : "3"
    },
    {
        "codigo" : "015",
         "modelo": "TRAILBLAZER",
         "marca" :"CHEVROLET",
         "año"   : "2024",
         "precio": "$33,175",
         "concesionario"  : "DISTRIBUIDORA LUMO C.A",
         "cant_disponible":"4",
         "ventas" : "1"
    },
    {
        "codigo" : "016",
         "modelo": "hatchback ",
         "marca" :"MAZDA",
         "año"   : "2023",
         "precio": "60.000",
         "concesionario"  : "KOBE MOTORS, C.A",
         "cant_disponible":"6",
         "ventas" : "3"
    },  {
        "codigo" : "017",
         "modelo": "cx-70",
         "marca" :"MAZDA",
         "año"   : "2024",
         "precio": "54.400",
         "concesionario"  : "KANSEI MOTORS, C.A",
         "cant_disponible":"4",
         "ventas" : "1"
    },  {
        "codigo" : "018",
         "modelo": "mx-5-miata-rf ",
         "marca" :"MAZDA",
         "año"   : "2023",
         "precio": "$37.000",
         "concesionario"  : "DISTRIBUIDORA LUMO C.A",
         "cant_disponible":"5",
         "ventas" : "1"
    }]
    )
} );


app.get("/ventas",(req,res)=> {
    res.send({
        "Productos": [
        {
        "mes": "Diciembre",
        "KOBE MOTORS, C.A": 20,
        "KANSEI MOTORS, C.A": 15,
        "DISTRIBUIDORA LUMO C.A": 10
        },
        {
            "mes": "Enero",
            "KOBE MOTORS, C.A": 12,
            "KANSEI MOTORS, C.A": 3,
            "DISTRIBUIDORA LUMO C.A": 9
            },
         {
                "mes": "Febrero",
                "KOBE MOTORS, C.A": 8,
                "KANSEI MOTORS, C.A": 7,
                "DISTRIBUIDORA LUMO C.A": 2
        }
              
        ]
        })
} );


app.get("/detalles",(req,res)=> {
    res.send({
        
            "productItems": [
                {
                    "title": "Detalles del vehiculo",
                    "marca": " TOYOTA",
                    "modelo": "YARIS CROSS",
                    "color": "Azul",
                    "motor": "NR 1,5L Dual VVT-i",
                    "capacidad de tanque": "42L",
                    "aire acondicionado": "AUTOMATICO",
                    "cauchos": "215/55 R18"
                },
            /*   {
                   
                    "title": "Detalles del vehiculo",
                    "marca": "CHEVROLET",
                    "modelo": "BOLT EUV",
                    "color": "Negro",
                    "motor": "NR 1,5L Dual VVT-i",
                    "capacidad de tanque": "42L",
                    "aire acondicionado": "AUTOMATICO",
                    "cauchos": "215/55 R18"
                },
                {
                  
                    "title": "Detalles del vehiculo",
                    "marca": "Mazda",
                    "modelo": "cx-70",
                    "color": "Rojo",
                    "motor": "NR 1,5L Dual VVT-i",
                    "capacidad de tanque": "42L",
                    "aire acondicionado": "AUTOMATICO",
                    "cauchos": "215/55 R18"
                }*/
            ]
        
    })
} );



app.listen(port,()=>{
    console.log(`Escuchando en el puerto: ${port}`);
})